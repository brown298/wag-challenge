<?php 
require_once __DIR__ . '/../app/autoload.php';

use johnbrown\Framework\Container;
use johnbrown\Framework\Kernel;

$container = new Container();
$kernel = new Kernel($container);

$request = $kernel->getRequestFromGlobals();
$response = $kernel->handle($request);
$response->send();
