**High Scores - Scoreboard**
----
This endpoint returns a collection of data needed to create the Scoreboard.

* **URL**

  /api/v1/scoreboard

* **Method:**
  
   `GET`
  
* **Data Params** 

   none

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** `{"total_players":"<countOfPlayer>","active_players":"<countOfCurrentDaysPlayers>","top_players":[{"id":"<playerId>","user_id":"<playerUserId>","score":"<score>","date_modified":"2017-07-28 19:33:13.000000"}, ...],"most_improved_players":[{"user_id":"10000320004499","this_week_score":"<thisWeeksScore>","last_week_score":"<scoreLastWeek>","score_diff":"<scoreImprovement"}, ...]}`
 
* **Error Response:**

  * **Code:** 500 Unexpected Database Error <br />
    **Content:** `{ "success" : false, "reason" : "Unexpected Database Error" }`

