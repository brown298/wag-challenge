**High Scores - Add Score**
----
This endpoint adds new score records for the current user

* **URL**

  /api/v1/score

* **Method:**
  
   `POST` | `PUT`
  
* **Data Params** 

   `user_score=[big integer]` 
   Score to be stored for the user
   
   `signed_request=[string]`
   Facebook signed Request

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** `{"success": true}`
 
* **Error Response:**

  * **Code:** 400 Missing argument user_score<br />
    **Content:** `{ "success" : false, "reason" : "Missing argument user_score" }`
  * **Code:** 403 Bad Signature<br />
    **Content:** `{ "success" : false, "reason" : "Bad Signature" }`
  * **Code:** 500 unknown error saving to database <br />
    **Content:** `{ "success" : false, "reason" : "unknown error saving to database" }`
