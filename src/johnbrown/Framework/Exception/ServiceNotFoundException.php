<?php
namespace johnbrown\Framework\Exception;

/**
 * Class ServiceNotFoundException
 *
 * could not locate a service
 *
 * @package johnbrown
 * @subpackage Framework\Exception
 */
class ServiceNotFoundException extends \Exception
{

}