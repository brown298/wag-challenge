<?php
namespace johnbrown\Framework\Exception;

/**
 * Class MissingArgumentException
 *
 * A required argument is missing
 * @package johnbrown
 * @subpackage Framework\Exception
 */
class MissingArgumentException extends \Exception
{

}