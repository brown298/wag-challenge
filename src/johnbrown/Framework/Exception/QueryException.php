<?php
namespace johnbrown\Framework\Exception;

/**
 * Class QueryException
 *
 * generic exeption for query errors
 * @package johnbrown
 * @subpackage Framework\Exception
 */
class QueryException extends \Exception
{

}