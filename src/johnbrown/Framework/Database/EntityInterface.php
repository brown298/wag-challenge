<?php
namespace johnbrown\Framework\Database;

/**
 * Entity Interface
 *
 * Defines an active record entity
 *
 * @package johnbrown
 * @subpackage Framework\Database
 */ 
interface EntityInterface
{
    /**
     * setConnection
     * @param \PDO $connection
     */
    public function setConnection(\PDO $connection);

    /**
     * getConnection
     * @return \PDO|null
     */
    public function getConnection();

    /**
     * @return string
     */
    public function getTableName();

    /**
     * @return mixed
     */
    public function createTable();

    /**
     * @return mixed
     */
    public function save();

    /**
     * @return mixed
     */
    public function remove();

    /**
     * @param array $data
     * @return mixed
     */
    public function load(array $data);
}
