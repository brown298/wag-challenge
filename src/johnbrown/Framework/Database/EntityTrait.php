<?php
namespace johnbrown\Framework\Database;

/**
 * Entity Trait
 *
 * ensures an entity can take a pdo connection
 *
 * @package johnbrown
 * @subpackage Framework\Database
 */
trait EntityTrait
{
    /**
     * @var PDO $connection
     */
    private $connection;

    /**
     * setConnection
     * @param \PDO $connection
     */
    public function setConnection(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * getConnection
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }
}
