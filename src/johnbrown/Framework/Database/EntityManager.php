<?php
namespace johnbrown\Framework\Database;

/**
 * Class EntityManager
 *
 * manages our entities and applies changes to database.
 * @package johnbrown
 * @subpackage Framework\Database
 */
class EntityManager
{
    /**
     * @var \PDO
     */
    public $connection;

    /**
     * EntityManager constructor.
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * saves the specified entity to the database;
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity)
    {
        $entity->setConnection($this->connection);
        return $entity->save();
    }

    /**
     * creates the specified table for the entity
     * @param EntityInterface $entity
     * @return bool
     */
    public function createTable(EntityInterface $entity)
    {
        $entity->setConnection($this->connection);
        return $entity->createTable();
    }

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function remove(EntityInterface $entity)
    {
        $entity->setConnection($this->connection);
        return $entity->remove();
    }

    /**
     * @param $statement
     * @param array $options
     * @return \PDOStatement
     */
    public function prepare($statement, array $options = [])
    {
        return $this->connection->prepare($statement, $options);
    }

    /** fetchAll
     * @param EntityInterface $entity
     * @param \PDOStatement|null $statement
     * @return array
     */
    public function fetchAll(EntityInterface $entity, \PDOStatement $statement = null)
    {
        if ($statement == null) {
            $statement = $this->connection->prepare("SELECT * FROM `" . $entity->getTable() . "`");
        }
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_CLASS, get_class($entity));
    }

    /**
     * @param EntityInterface $entity
     * @param \PDOStatement|null $statement
     * @return mixed
     */
    public function fetchOne(EntityInterface $entity, \PDOStatement $statement = null)
    {
        if ($statement == null) {
            $statement = $this->connection->prepare("SELECT * FROM `" . $entity->getTable() . "` LIMIT 1");
        }
        $statement->setFetchMode(\PDO::FETCH_CLASS, get_class($entity));
        $statement->execute();
        return $statement->fetch();
}

    /**
     * @param \PDOStatement $statement
     * @return array
     */
    public function fetchAllAssoc(\PDOStatement $statement)
    {
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * @param \PDOStatement $statement
     * @return array
     */
    public function fetchAssoc(\PDOStatement $statement)
    {
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        $statement->execute();
        return $statement->fetch();
    }

}