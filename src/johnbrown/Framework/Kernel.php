<?php 
namespace johnbrown\Framework;
use johnbrown\Framework\Messages\Interfaces\RequestInterface;
use johnbrown\Framework\Messages\Interfaces\ResponseInterface;
use johnbrown\Framework\Messages\ServerResponse;
use johnbrown\Framework\Route\Matcher;
use johnbrown\Framework\Route\RouteBuilderInterface;
use johnbrown\Framework\Route\RouterInterface;

/**
 * Class Kernel
 *
 * kernel for this custom framework
 *
 * @package johnbrown
 * @subpackage Framework
 */
class Kernel
{
    const SERVICE_NAME = 'kernel';
    /**
     * @var Container
     */
    private $container;

    /**
     * Kernel constructor.
     * @param Container $container
     */
    public function __construct(
        Container $container
    )
    {
        $this->container = $container;
        $this->container->add(self::SERVICE_NAME, $this);
    }

    /**
     * @return RequestInterface
     */
    public function getRequestFromGlobals()
    {
        $requestBuilder = $this->container->get('request_builder');
        $request = $requestBuilder->createFromGlobals();
        return $request;
    }

    /**
     * @param RouterInterface $router
     * @return mixed
     */
    private function buildRoutes(RouterInterface $router)
    {
        /** @var RouteBuilderInterface $builder */
        $builder = $this->container->get('route_builder');
        return $builder->buildRoutes($router);
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function handle(RequestInterface $request)
    {
        // build routes
        $router = $this->container->get('router');
        $this->buildRoutes($router);

        /** @var Matcher $matcher */
        $matcher = $this->container->get('route_matcher');

        /** @var callable $action */
        $action = $matcher->match($request);

        if (is_callable($action)) {
            return $action($request);
        }

        return (new ServerResponse())
            ->withStatus(404, 'not found');
    }
}
