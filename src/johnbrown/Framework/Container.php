<?php
namespace johnbrown\Framework;

use johnbrown\App\Controller\DefaultController;
use johnbrown\App\RouteBuilder;
use johnbrown\App\Service\MockDataService;
use johnbrown\App\Service\PlayerInfoService;
use johnbrown\Client\Client;
use johnbrown\Facebook\Service\SignedRequestService;
use johnbrown\Framework\Database\EntityManager;
use johnbrown\Framework\Exception\ServiceNotFoundException;
use johnbrown\Framework\Messages\RequestBuilder;
use johnbrown\Framework\Route\Matcher;
use johnbrown\Framework\Route\Router;

/**
 * Class Container
 *
 * A dependency injection container to store parameters and services
 *
 * @package johnbrown
 * @subpackage Framework
 */
class Container implements ContainerInterface
{
    /**
     * @var object[]
     */
    private $services = [];

    /**
     * @var array
     */
    private $parameters = [
        'host'                       => 'johnbrown.life',
        'facebook.appId'             => '126767144061773',
        'facebook.secret'            => "21db65a65e204cca7b5afcbad91fea59",
        'db.connection'              => "mysql:host=192.168.1.103;dbname=scores",
        'db.user'                    => "high",
        'db.password'                => "scores",
        'scoreboard.display_players' => 10,
    ];

    /**
     * @param $id
     * @return object
     * @throws ServiceNotFoundException
     */
    public function get($id)
    {
        if (isset($this->services[$id])) return $this->services[$id];

        $method = 'get' . $this->camelize($id);

        if (method_exists($this, $method)) {
            return $this->$method();
        }
        throw new ServiceNotFoundException("Could not find service with id " . $id . ' or method ' . $method);
    }

    /**
     * add
     *
     * allows a service to be registered
     *
     * @param $id
     * @param $object
     * @return static
     */
    public function add($id, $object)
    {
        $this->services[$id] = $object;
        return $this;
    }


    /**
     * @param $name
     * @return bool
     */
    public function hasParameter($name)
    {
        return isset($this->parameters[$name]);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function getParameter($name)
    {
        if (!$this->hasParameter($name)) return null;
        return $this->parameters[$name];
    }

    /**
     * @param $name
     * @param $value
     */
    public function setParameter($name, $value)
    {
        $this->parameters[$name] = $value;
    }

    /**
     * converts a string from underscore to camel case
     *
     * @param string $id
     * @return string
     */
    public function camelize($id)
    {
        return strtr(ucwords(strtr($id, array('_' => ' ', '.' => '_ ', '\\' => '_ '))), array(' ' => ''));
    }

    /**
     * converts a camelized string to underscore
     * @param string $id The string to underscore
     * @return string The underscored string
     */
    public function underscore($id)
    {
        return strtolower(preg_replace(array('/([A-Z]+)([A-Z][a-z])/', '/([a-z\d])([A-Z])/'), array('\\1_\\2', '\\1_\\2'), str_replace('_', '.', $id)));
    }

    /**
     * getRequestBuilder
     *
     * @return RequestBuilder
     */
    public function getRequestBuilder()
    {
        return $this->services['request_builder'] = new RequestBuilder();
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        return $this->services['router'] = new Router();
    }

    /**
     * @return Matcher
     */
    public function getRouteMatcher()
    {
        return $this->services['route_matcher'] = new Matcher(
            $this->get('router')
        );
    }

    /**
     * @return RouteBuilder
     */
    public function getRouteBuilder()
    {
        $controllersToLoad = [
            'app.default_controller',
        ];
        $controllers = $this->getControllers($controllersToLoad);
        return $this->services['route_builder'] = new RouteBuilder(
            $controllers
        );
    }

    /**
     * @param array $controllersToLoad
     * @return array
     */
    public function getControllers(array $controllersToLoad = [])
    {
        $controllers = [];
        foreach ($controllersToLoad as $name) {
            $controllers[$name] = $this->get($name);
        }
        return $controllers;
    }

    /**
     * @return DefaultController
     */
    public function getApp_DefaultController()
    {
        $this->services['app.default_controller'] = new DefaultController();
        $this->services['app.default_controller']->setContainer($this);
        return $this->services['app.default_controller'];
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->services['client'] = new Client();
    }

    /**
     * getDatabaseConnection
     * @return PDO
     */
    public function getDatabaseConnection()
    {
        return $this->services['database_connection'] = new \PDO(
            $this->getParameter('db.connection'),
            $this->getParameter('db.user'),
            $this->getParameter('db.password')
        );
    }

    /**
     * @return SignedRequestService
     */
    public function getFacebook_SignedRequestService()
    {
        return $this->services['facebook.signed_request_service'] = new SignedRequestService(
            $this->getParameter('facebook.appId'),
            $this->getParameter('facebook.secret')
        );
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->services['entity_manager'] = new EntityManager(
            $this->get('database_connection')
        );
    }

    /**
     * @return PlayerInfoService
     */
    public function getPlayerInfoService()
    {
        return $this->services['player_info_service'] = new PlayerInfoService(
            $this->get('entity_manager')
        );
    }

    /**
     * @return MockDataService
     */
    public function getMockDataService()
    {
        return $this->services['mock_data_service'] = new MockDataService(
            $this->get('entity_manager'),
            $this->get('client'),
            $this->get('facebook.signed_request_service'),
            $this->getParameter('host')
        );
    }
}
