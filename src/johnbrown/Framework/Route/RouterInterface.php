<?php
namespace johnbrown\Framework\Route;

use johnbrown\Framework\Route\Exception\RouteExistsException;

/**
 * Interface RouterInterface
 * @package johnbrown\Framework\Route
 */
interface RouterInterface
{
    /**
     * addRoute
     * adds a route to the available routes
     *
     * @param string   $path     path to match againse
     * @param callable $func     action to implement on matched route
     * @param array    $methods  acceptable methods
     * @param array    $defaults default values
     * @throws RouteExistsException
     * @return static
     */
    public function addRoute($path, callable $func, array $methods = [], array $defaults = []);

    /**
     * @return array
     */
    public function getRoutes();
}