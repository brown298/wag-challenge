<?php 
namespace johnbrown\Framework\Route;

use johnbrown\Framework\Route\Exception\RouteExistsException;

/**
 * Router Class
 * stores my implementation of a router
 */
class Router implements RouterInterface
{
    /**
      * @var $routes array of available routes
      */
    private $routes = [];

    /**
      * addRoute
      * adds a route to the available routes
      *
      * @param string   $path     path to match againse
      * @param callable $func     action to implement on matched route
      * @param array    $methods  acceptable methods
      * @param array    $defaults default values
      * @throws RouteExistsException
      * @return static
      */
    public function addRoute($path, callable $func, array $methods = [], array $defaults = [])
    {
        if (isset($this->routes[$path])) {
            throw new RouteExistsException('Error trying to add route ' . $path . ' route already exists');
        }

        $this->routes[$path] = [
            'callable' => $func,
            'methods'  => $methods,
            'defaults' => $defaults,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }
}
