<?php 
namespace johnbrown\Framework\Route;

use johnbrown\Framework\Messages\Request;

/**
 * Matcher
 * attempts to match a request to a route
 */
class Matcher
{
    /**
     * @var Router
     */
    private $router;

    /**
     * Matcher constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * matches and returns the router record for a request
     * @param  Request $request
     * @return bool|array
     */
    public function match(Request $request)
    {
        $routes = $this->router->getRoutes();
        foreach($routes as $path => $routeInfo) {
            if (!preg_match($path, $request->getUri()->getPath())) {
                continue; // path is wrong
            }

            if (!in_array($request->getMethod(), $routeInfo['methods'])) {
                continue; // method is wrong
            }

            // we have a matched route
            return $routeInfo['callable'];
        }
        return false;
    }
}
