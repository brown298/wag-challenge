<?php
namespace johnbrown\Framework\Route;

/**
 * Interface RouteBuilderInterface
 * @package johnbrown
 * @subpackage Framework\Route
 */
interface RouteBuilderInterface
{

    /**
     * define all the application routes here
     * @param RouterInterface $router
     * @return mixed
     */
    public function buildRoutes(RouterInterface $router);

}