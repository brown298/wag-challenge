<?php
namespace johnbrown\Framework\Route\Exception;

/**
 * Exception indicating a route already exists for a path name
 */
class RouteExistsException extends \Exception
{
}
