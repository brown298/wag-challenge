<?php
namespace johnbrown\Framework;
use johnbrown\Framework\Messages\Interfaces\RequestInterface;
use johnbrown\Framework\Route\RouterInterface;

/**
 * Class AbstractController
 *
 * ensures the controller gets the container and can build routes
 *
 * @package johnbrown
 * @subpackage Framework
 */
abstract class AbstractController
{
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var ContainerInterface
     */
    private $container = null;

    /**
     * setContainer
     *
     * @param ContainerInterface $container
     * @return $this
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @return ContainerInterface|null
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param RouterInterface $router
     */
    public function buildRoutes(RouterInterface $router)
    {
        foreach ($this->routes as $routeConfig) {
            if (isset($routeConfig['path']) && isset($routeConfig['action']) && isset($routeConfig['methods'])) {
                $defaults = (isset($routeConfig['defaults'])) ? $routeConfig['defaults'] : [];
                $router->addRoute($routeConfig['path'], [$this, $routeConfig['action']], $routeConfig['methods'], $defaults);
            }
        }
    }

}