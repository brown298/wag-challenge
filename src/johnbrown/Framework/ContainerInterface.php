<?php
namespace johnbrown\Framework;

/**
 * Interface ContainerInterface
 * @package johnbrown
 * @subpackage Framework
 */
interface ContainerInterface
{

    /**
     * @param $id
     * @return object
     * @throws ServiceNotFoundException
     */
    public function get($id);

    /**
     * add
     *
     * allows a service to be registered
     *
     * @param $id
     * @param $object
     * @return static
     */
    public function add($id, $object);
}