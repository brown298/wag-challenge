<?php 
namespace johnbrown\Framework\Messages;
use johnbrown\Framework\Messages\Interfaces\UriInterface;

/**
 * Request
 *
 * standardized request object based on PSR-7 http request interface but not
 * implementing as it is in an external library
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class ServerRequest extends Request
{
    /**
     * @var array
     */
    private $attributes = [];

    /**
     * @var array
     */
    private $cookieParams = [];

    /**
     * @var null|array|object
     */
    private $parsedBody;

    /**
     * @var array
     */
    private $queryParams = [];

    /**
     * @var array
     */
    private $serverParams;

    /**
     * @var array
     */
    private $uploadedFiles;

    /**
     * ServerRequest constructor.
     * @param array $serverParams
     * @param array $uploadedFiles
     * @param UriInterface $uri
     * @param null $method
     * @param string $body
     * @param array $headers
     * @param array $cookies
     * @param array $queryParams
     * @param null $parsedBody
     * @param string $protocol
     */
    public function __construct(
        array $serverParams = [],
        array $uploadedFiles = [],
        UriInterface $uri = null,
        $method = null,
        $body = "php://input",
        array $headers = [],
        array $cookies = [],
        array $queryParams = [],
        $parsedBody = null,
        $protocol = '1.1'
    )
    {
        $this->serverParams = $serverParams;
        $this->uploadedFiles = $uploadedFiles;
        $this->cookieParams = $cookies;
        $this->queryParams = $queryParams;
        $this->parsedBody = $parsedBody;
        $this->protocol = $protocol;
        parent::__construct($uri, $method);
    }

    /**
     * @return array
     */
    public function getServerParams()
    {
        return $this->serverParams;
    }

    /**
     * @return array
     */
    public function getCookieParams()
    {
        return $this->cookieParams;
    }

    /**
     * @param array $cookies
     * @return ServerRequest
     */
    public function withCookieParams(array $cookies)
    {
        $new = clone $this;
        $new->cookieParams = $cookies;
        return $new;
    }

    /**
     * @return array
     */
    public function getQueryParams()
    {
        return $this->queryParams;
    }

    /**
     * @param array $params
     * @return ServerRequest
     */
    public function withQueryParams(array $params)
    {
        $new = clone $this;
        $new->queryParams = $params;
        return $new;
    }

    /**
     * @return array
     */
    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    /**
     * @param array $uploadedFiles
     * @return ServerRequest
     */
    public function withUploadedFiles(array $uploadedFiles)
    {
        $new = clone $this;
        $new->uploadedFiles =$uploadedFiles;
        return $new;
    }

    /**
     * @return array|null|object
     */
    public function getParsedBody()
    {
        return $this->parsedBody;
    }

    /**
     * @param $data
     * @return ServerRequest
     */
    public function withParsedBody($data)
    {
        $new = clone $this;
        $new->parsedBody = $data;
        return $new;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param $name
     * @param null $default
     * @return mixed|null
     */
    public function getAttribute($name, $default=null)
    {
        $name = strtolower($name);
        if (isset($this->attributes[$name])) return $this->attributes[$name];
        return $default;
    }

    /**
     * @param $name
     * @param $value
     * @return ServerRequest
     */
    public function withAttribute($name, $value)
    {
        $name = strtolower($name);
        $new = clone $this;
        $new->attributes[$name] = $value;
        return $new;
    }

    /**
     * @param $name
     * @return ServerRequest
     */
    public function withoutAttribute($name)
    {
        $name = strtolower($name);
        $new = clone $this;
        unset($new->attributes[$name]);
        return $new;
    }

    /**
     * @return array
     */
    public function getPathParams()
    {
        $path = $this->getUri()->getPath();
        return explode('/', $path);
    }
}
