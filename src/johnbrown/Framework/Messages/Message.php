<?php 
namespace johnbrown\Framework\Messages;
use johnbrown\Framework\Messages\Interfaces\MessageInterface;
use johnbrown\Framework\Messages\Interfaces\StreamInterface;

/**
 * Message
 *
 * standardized request object based on PSR-7 http request interface but not
 * implementing as it is in an external library
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class Message implements MessageInterface
{
    /**
     * List of all registered headers, as key => array of values.
     *
     * @var array
     */
    protected $headers = [];

    /**
     * Map of normalized header name to original name used to register header.
     *
     * @var array
     */
    protected $headerNames = [];

    /**
     * @var string
     */
    protected $protocol = '1.1';

    /**
     * @var StreamInterface
     */
    private $stream;

    /**
     * retrieves the http protocol version as a string
     * @return string
     */
    public function getProtocolVersion()
    {
        return $this->protocol;
    }

    /**
     * Returns an instance with the specified protocol version
     * @param string $version
     * @return static
     */
    public function withProtocolVersion($version)
    {
        $new = clone $this;
        $new->protocol = $version;
        return $new;
    }

    /**
     * getHeaders
     *
     * @return string[][]
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasHeader($name)
    {
        $name = strtolower($name);
        return isset($this->headers[$name]);
    }

    /**
     * @param $name
     * @return array|mixed
     */
    public function getHeader($name)
    {
        $name = strtolower($name);
        return ($this->hasHeader($name)) ? $this->headers[$name] : [];
    }

    /**
     * @param $name
     * @param $value
     * @return static
     */
    public function withHeader($name, $value)
    {
        $name = strtolower($name);
        $new = clone $this;

        if ($new->hasHeader($name)) {
            unset($new->headers[$name]);
        }
        $new->headers[$name] = $value;

        return $new;
    }

    /**
     * @param $name
     * @param $value
     * @return static
     */
    public function withAddedHeader($name, $value)
    {
        $name = strtolower($name);
        if (!$this->hasHeader($name)) {
            return $this->withHeader($name, $value);
        }

        $new = clone $this;

        $new->headers[$name] = array_merge($this->headers[$name], $value);
        return $new;
    }

    /**
     * @param $name
     * @return static
     */
    public function withoutHeader($name)
    {
        $name = strtolower($name);
        $new = clone $this;
        if ($this->hasHeader($name)) {
            unset($new->headers[$name]);
        }
        return $new;
    }

    /**
     * @return StreamInterface
     */
    public function getBody()
    {
        return $this->stream;
    }

    /**
     * @param StreamInterface $body
     * @return mixed|void
     */
    public function withBody(StreamInterface $body)
    {
        $new = clone $this;
        $new->stream = $body;
        return $new;
    }
}
