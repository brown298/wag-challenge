<?php
namespace johnbrown\Framework\Messages;
use johnbrown\Framework\Messages\Interfaces\UriInterface;

/**
 * Class RequestBuilder
 *
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class RequestBuilder
{

    /**
     * create Request
     * @param array $query
     * @param array $body
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @return ServerRequest
     */
    public function createRequest(
        array $query      = [],
        array $body       = [],
        array $attributes = [],
        array $cookies    = [],
        array $files      = [],
        array $server     = []
    )
    {
        $request = new ServerRequest(
            $server,
            $files,
            $this->createUri($server),
            $this->getMethod($server),
            'php://input',
            $this->getHeaders($server),
            $cookies,
            $query,
            $body,
            $this->getProtocol($server) // getProtocolVersion
        );

        return $request;
    }

    /**
     * @param $server
     * @return UriInterface
     */
    private function createUri($server)
    {
        $uri = new Uri();
        $scheme = 'http';

        $uri = $uri->withScheme($scheme);

        $query = (isset($server['QUERY_STRING'])) ? ltrim($server['QUERY_STRING'], '?') : '';

        $host = $this->getHost($server);
        $port = $this->getPort($server);
        if (! empty($host)) {
            $uri = $uri->withHost($host);
            if (! empty($port)) {
                $uri = $uri->withPort($port);
            }
        }

        $path = $this->parsePath($server);

        $fragment = '';
        if (strpos($path, '#') !== false) {
            list($path, $fragment) = explode('#', $path, 2);
        }

        return $uri
            ->withPath($path)
            ->withFragment($fragment)
            ->withQuery($query)
            ;
    }

    /**
     * @param $server
     * @return string
     */
    private function getHost($server)
    {
        return (isset($server['SERVER_NAME'])) ? $server['SERVER_NAME'] : '';
    }

    /***
     * @param $server
     * @return int|null
     */
    private function getPort($server)
    {
        return (isset($server['SERVER_PORT'])) ? intval($server['SERVER_PORT']) : null;
    }

    /**
     * @param $server
     * @return string
     */
    private function getMethod($server)
    {
        return (isset($server['REQUEST_METHOD'])) ? $server['REQUEST_METHOD'] : 'get';
    }

    /**
     * @param $server
     * @return string
     */
    private function parsePath($server)
    {
        return (isset($server['REQUEST_URI'])) ? $server['REQUEST_URI'] : '';
    }

    /**
     * @param $server
     * @return array
     */
    public function getHeaders($server)
    {
        $headers = [];
        foreach ($server as $key => $value) {
            if (strpos($key, 'REDIRECT_') === 0) {
                $key = substr($key, 9);
            }
            if ($value && strpos($key, 'HTTP_') === 0) {
                $name = strtr(strtolower(substr($key, 5)), '_', '-');
                $headers[$name] = $value;
                continue;
            }
            if ($value && strpos($key, 'CONTENT_') === 0) {
                $name = 'content-' . strtolower(substr($key, 8));
                $headers[$name] = $value;
                continue;
            }
        }
        return $headers;
    }

    /**
     * createFromGlobals
     *
     * @return ServerRequest
     */
    public function createFromGlobals()
    {
        $server = $_SERVER;
        if ('cli-server' === PHP_SAPI) { // account for php's built in web server
            if (array_key_exists('HTTP_CONTENT_LENGTH', $_SERVER)) {
                $server['CONTENT_LENGTH'] = $_SERVER['HTTP_CONTENT_LENGTH'];
            }
            if (array_key_exists('HTTP_CONTENT_TYPE', $_SERVER)) {
                $server['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
            }
        }

        return $this->createRequest($_GET, $_POST, [], $_COOKIE, $_FILES, $server);
    }

    /**
     * @param $server
     * @return string
     */
    private function getProtocol($server)
    {
        if (! isset($server['SERVER_PROTOCOL'])) {
            return '1.1';
        }
        if (! preg_match('#^(HTTP/)?(?P<version>[1-9]\d*(?:\.\d)?)$#', $server['SERVER_PROTOCOL'], $matches)) {
            return '1.1';
        }
        return $matches['version'];
    }
}

