<?php
namespace johnbrown\Framework\Messages;
use johnbrown\Framework\Messages\Interfaces\ResponseInterface;

/**
 * Class Response
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class Response
{
    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var string
     */
    private $reasonPhrase;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var
     */
    private $content;

    /**
     * Response constructor.
     * @param $content
     * @param int $statusCode
     * @param string $reasonPhrase
     * @param array $headers
     */
    public function __construct($content, $statusCode=200, $reasonPhrase='Success', $headers = [])
    {
        $this->content = $content;
        $this->statusCode = $statusCode;
        $this->reasonPhrase = $reasonPhrase;
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * @return mixed
     */
    public function send()
    {
        $stream = new Stream('php://memory', 'rw');
        $stream->write($this->getContent());

        $response = (new ServerResponse())
            ->withStatus($this->statusCode, $this->reasonPhrase)
            ->withBody($stream)
        ;

        foreach ($this->headers as $name => $header) {
            $response = $response->withHeader($name, $header);
        }

        $this->sendResponse($response);
    }

    /**
     * @param ServerResponse $response
     */
    public function sendResponse(ServerResponse $response)
    {
        if (!headers_sent()) {
            // status
            header(sprintf('HTTP/%s %s %s', $response->getProtocolVersion(), $response->getStatusCode(), $response->getReasonPhrase()), true, $response->getStatusCode());
            // headers
            foreach ($response->getHeaders() as $header => $values) {
                foreach ($values as $value) {
                    header($header.': '.$value, false, $response->getStatusCode());
                }
            }
        }
        echo $response->getBody();
    }
}