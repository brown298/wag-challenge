<?php
namespace johnbrown\Framework\Messages;
use johnbrown\Framework\Messages\Interfaces\ResponseInterface;

/**
 * Class Response
 *
 * standardized request object based on PSR-7 http request interface but not
 * implementing as it is in an external library
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class ServerResponse extends Message implements ResponseInterface
{
    /**
     * @var int 3 digit status code
     */
    private $statusCode = 200;

    /**
     * @var string string for reason
     */
    private $reasonPhrase = '';

    /**
     * getStatusCode
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * withStatus
     * @param int $code
     * @param string $reasonPhrase
     * @return static
     */
    public function withStatus($code, $reasonPhrase = '')
    {
        $new = clone $this;
        $new->statusCode = $code;
        $new->reasonPhrase = $reasonPhrase;
        return $new;
    }

    /**
     * getReasonPhrase
     *
     * @return string
     */
    public function getReasonPhrase()
    {
        return $this->reasonPhrase;
    }

    /**
     * stub
     */
    public function send() {}
}
