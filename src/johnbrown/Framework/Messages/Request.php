<?php
namespace johnbrown\Framework\Messages;
use johnbrown\Framework\Messages\Interfaces\RequestInterface;
use johnbrown\Framework\Messages\Interfaces\UriInterface;

/**
 * Request
 *
 * standardized request object based on PSR-7 http request interface but not
 * implementing as it is in an external library
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class Request extends Message implements RequestInterface
{
    /**
     * @var UriInterface
     */
    private $uri;

    /**
     * @var string
     */
    private $method;

    /**
     * @var null|string
     */
    private $requestTarget;

    public function __construct(
        UriInterface $uri,
        $method = 'get',
        $requestTarget = null
    )
    {
        $this->uri = $uri;
        $this->method = $method;
        $this->requestTarget = $requestTarget;
    }

    /**
     * @return null|string
     */
    public function getRequestTarget()
    {
        return $this->requestTarget;
    }

    /**
     * @param $requestTarget
     * @return static
     */
    public function withRequestTarget($requestTarget)
    {
        $new = clone $this;
        $new->requestTarget = $requestTarget;
        return $new;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param $method
     * @return static
     */
    public function withMethod($method)
    {
        $new = clone $this;
        $new->method = $method;
        return $new;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param UriInterface $uri
     * @param bool $preserveHost
     * @return static
     */
    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        $new = clone $this;
        $new->uri = $uri;

        if ($preserveHost && $this->hasHeader('Host')) {
            return $new;
        }

        if (! $uri->getHost()) {
            return $new; // nothing to replace
        }

        $newHost = $uri->getHost();
        if ($uri->getPort()) $newHost .= ':' . $uri->getPort();
        $new->headerNames['host'] = 'Host';
        $new->headers['Host'] = [$newHost];

        return $new;
    }

}
