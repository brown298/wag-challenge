<?php
namespace johnbrown\Framework\Messages;

/**
 * Class JsonResponse
 *
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class JsonResponse extends Response
{
    /**
     * @return mixed
     */
    public function getContent()
    {
        return json_encode(parent::getContent());
    }
}