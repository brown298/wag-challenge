<?php
namespace johnbrown\Framework\Messages\Interfaces;

/**
 * Interface ResponseInterface
 *
 * @package johnbrown
 * @subpackage Framework\Messages\Interfaces
 */
interface ResponseInterface
{

    /**
     * getStatusCode
     *
     * @return int
     */
    public function getStatusCode();
    /**
     * withStatus
     * @param int $code
     * @param string $reasonPhrase
     * @return static
     */
    public function withStatus($code, $reasonPhrase = '');

    /**
     * getReasonPhrase
     *
     * @return string
     */
    public function getReasonPhrase();
}