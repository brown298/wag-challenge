<?php
namespace johnbrown\Framework\Messages\Interfaces;

/**
 * Interface MessageInterface
 * @package johnbrown
 * @subpackage Framework\Messages\Interfaces
 */
interface MessageInterface
{
    /**
     * @return mixed
     */
    public function getProtocolVersion();

    /**
     * Returns an instance with the specified protocol version
     * @param string $version
     * @return static
     */
    public function withProtocolVersion($version);

    /**
     * getHeaders
     *
     * @return string[][]
     */
    public function getHeaders();

    /**
     * @param $name
     * @return mixed
     */
    public function hasHeader($name);

    /**
     * @param $name
     * @return mixed
     */
    public function getHeader($name);

    /**
     * @param $name
     * @param $value
     * @return mixed
     */
    public function withHeader($name, $value);

    /**
     * @param $name
     * @param $value
     * @return mixed
     */
    public function withAddedHeader($name, $value);

    /**
     * @param $name
     * @return mixed
     */
    public function withoutHeader($name);

    /**
     * @return mixed
     */
    public function getBody();

    /**
     * @param $body
     * @return mixed
     */
    public function withBody(StreamInterface $body);
}