<?php
namespace johnbrown\Framework\Messages\Interfaces;

/**'
 * Interface RequestInterface
 * @package johnbrown
 * @subpackage Framework\Messages\Interfaces
 */
interface RequestInterface
{
    /**
     * @return mixed
     */
    public function getRequestTarget();

    /**
     * @param $requestTarget
     * @return mixed
     */
    public function withRequestTarget($requestTarget);

    /**
     * @return mixed
     */
    public function getMethod();

    /**
     * @param $method
     * @return mixed
     */
    public function withMethod($method);

    /**
     * @return UriInterface
     */
    public function getUri();

    /**
     * @param $uri
     * @param bool $preserveHost
     * @return mixed
     */
    public function withUri(UriInterface $uri, $preserveHost = false);
}