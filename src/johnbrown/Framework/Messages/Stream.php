<?php
namespace johnbrown\Framework\Messages;

use johnbrown\Framework\Messages\Interfaces\StreamInterface;

/**
 * Class Stream
 *
 * standardized request object based on PSR-7 http request interface but not
 * implementing as it is in an external library
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class Stream implements StreamInterface
{
    /**
     * @var resource|null
     */
    protected $resource;

    /**
     * @var string|resource
     */
    protected $stream;

    /**
     * Stream constructor.
     * @param $stream
     * @param string $mode
     */
    public function __construct($stream, $mode ='r')
    {
        $this->setStream($stream, $mode);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            $this->rewind();
            return $this->getContents();
        } catch (\RuntimeException $e) {
            return '';
        }
    }

    /**
     * @return void
     */
    public function close()
    {
        if (! $this->resource) {
            return;
        }
        $resource = $this->detach();
        fclose($resource);
    }

    /**
     * @return resource|null
     */
    public function detach()
    {
        $resource = $this->resource;
        $this->resource = null;
        return $resource;
    }

    /**
     * @return int|null
     */
    public function getSize()
    {
        if (null === $this->resource) {
            return null;
        }
        $stats = fstat($this->resource);
        return $stats['size'];
    }

    /**
     * @return int
     * @throws \RuntimeException
     */
    public function tell()
    {
        if (! $this->resource) {
            throw new \RuntimeException('No resource available; cannot tell position');
        }
        $result = ftell($this->resource);
        if (! is_int($result)) {
            throw new \RuntimeException('Error occurred during tell operation');
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function eof()
    {
        if (! $this->resource) {
            return;
        }
        $resource = $this->detach();
        fclose($resource);
    }

    /**
     * @return bool
     */
    public function isSeekable()
    {
        if (! $this->resource) {
            return false;
        }
        $meta = stream_get_meta_data($this->resource);
        return $meta['seekable'];
    }

    /**
     * @param int $offset
     * @param int $whence
     * @throws \RuntimeException on failure.
     * @return bool
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if (! $this->resource) {
            throw new \RuntimeException('No resource available, cannot seek');
        }
        if (! $this->isSeekable()) {
            throw new \RuntimeException('Stream is not seekable');
        }
        $result = fseek($this->resource, $offset, $whence);
        if (0 !== $result) {
            throw new \RuntimeException('Error seeking');
        }
        return true;
    }

    /**
     * @throws \RuntimeException
     */
    public function rewind()
    {
        return $this->seek(0);
    }

    /**
     * @return bool
     */
    public function isWritable()
    {
        if (! $this->resource) {
            return false;
        }
        $meta = stream_get_meta_data($this->resource);
        $mode = $meta['mode'];
        return (
            strstr($mode, 'x')
            || strstr($mode, 'w')
            || strstr($mode, 'c')
            || strstr($mode, 'a')
            || strstr($mode, '+')
        );
    }

    /**
     * @param string $string
     * @return int
     * @throws \RuntimeException
     */
    public function write($string)
    {
        if (! $this->resource) {
            throw new \RuntimeException('No resource available, cannot write');
        }
        if (! $this->isWritable()) {
            throw new \RuntimeException('Stream is not writable');
        }
        $result = fwrite($this->resource, $string);
        if (false === $result) {
            throw new \RuntimeException('Error writing to stream');
        }

        return $result;

    }

    /**
     * @return bool
     */
    public function isReadable()
    {
        if (! $this->resource) {
            return false;
        }
        $meta = stream_get_meta_data($this->resource);
        $mode = $meta['mode'];
        return (strstr($mode, 'r') || strstr($mode, '+'));
    }

    /**
     * @param int $length
     * @return string
     * @throws \RuntimeException
     */
    public function read($length)
    {
        if (! $this->resource) {
            throw new \RuntimeException('No resource available, cannot read');
        }

        if (! $this->isReadable()) {
            throw new \RuntimeException('Stream is not readable');
        }

        $result = fread($this->resource, $length);
        if (false === $result) {
            throw new \RuntimeException('Error reading stream');
        }
        return $result;
    }

    /**
     * @return string
     * @throws \RuntimeException
     * @throws \RuntimeException
     */
    public function getContents()
    {
        if (! $this->isReadable()) {
            throw new RuntimeException('Stream is not readable');
        }
        $result = stream_get_contents($this->resource);
        if (false === $result) {
            throw new \RuntimeException('Error reading from stream');
        }
        return $result;
    }

    /**
     * @return array|mixed|null
     */
    public function getMetadata($key = null)
    {
        if (null === $key) {
            return stream_get_meta_data($this->resource);
        }
        $metadata = stream_get_meta_data($this->resource);
        if (! array_key_exists($key, $metadata)) {
            return null;
        }
        return $metadata[$key];
    }

    /**
     * @param $stream
     * @param string $mode
     */
    private function setStream($stream, $mode = 'r')
    {
        $error    = null;
        $resource = $stream;
        if (is_string($stream)) {
            set_error_handler(function ($e) use (&$error) {
                $error = $e;
            }, E_WARNING);
            $resource = fopen($stream, $mode);
            restore_error_handler();
        }
        if ($error) {
            throw new \InvalidArgumentException('Invalid stream reference provided');
        }
        if (! is_resource($resource) || 'stream' !== get_resource_type($resource)) {
            throw new \InvalidArgumentException(
                'Invalid stream provided; must be a string stream identifier or stream resource'
            );
        }
        if ($stream !== $resource) {
            $this->stream = $stream;
        }
        $this->resource = $resource;
    }
}