<?php
namespace johnbrown\Framework\Messages;
use johnbrown\Framework\Messages\Interfaces\UriInterface;

/**
 * Class Uri
 *
 * standardized request object based on PSR-7 http request interface but not
 * implementing as it is in an external library
 * @package johnbrown
 * @subpackage Framework\Messages
 */
class Uri implements UriInterface
{
    /**
     * @var string
     */
    private $scheme = '';

    /**
     * @var string
     */
    private $userInfo = '';

    /**
     * @var string
     */
    private $host = '';

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $path = '';

    /**
     * @var string
     */
    private $query = '';

    /**
     * @var string
     */
    private $fragment = '';

    /**
     * @var null
     */
    private $cachedUriString = null;

    /**
     * @return string
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @param string $scheme
     * @return static
     */
    public function withScheme($scheme)
    {
        $new = clone $this;
        $new->scheme = $scheme;
        return $new;
    }

    /**
     * @return string
     */
    public function getAuthority()
    {
        return ''; // not needed for this implementation
    }

    /**
     * @return string
     */
    public function getUserInfo()
    {
        return $this->userInfo;
    }

    /**
     * @param string
     * @param null|string $password
     * @return static
     */
    public function withUserInfo($user, $password = null)
    {
        return $this; // not needed for this implementation
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return static
     * @throws \InvalidArgumentException
     */
    public function withHost($host)
    {
        $new = clone $this;
        $new->host = $host;
        return $new;
    }

    /**
     * @return null|int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return string
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * @param null|int $port
     * @return static
     * @throws \InvalidArgumentException
     */
    public function withPort($port)
    {
        $new = clone $this;
        $new->port = $port;
        return $new;
    }

    /**
     * @param string $path
     * @return static
     * @throws \InvalidArgumentException
     */
    public function withPath($path)
    {
        $new = clone $this;
        $new->path = $path;
        return $new;
    }

    /**
     * @param string $query
     * @return static
     * @throws \InvalidArgumentException
     */
    public function withQuery($query)
    {
        $new = clone $this;
        $new->query = $query;
        return $new;
    }

    /**
     * @param string $fragment
     * @return static
     */
    public function withFragment($fragment)
    {
        $new = clone $this;
        $new->fragment = $fragment;
        return $new;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->cachedUriString !== null) return $this->cachedUriString;

        $uriString = '';
        $scheme = $this->getScheme();
        if (! empty($scheme)) {
            $uriString .= sprintf('%s:', $this->getScheme());
        }

        $host = $this->getHost();
        if (! empty($host)) {
            $uriString .= '//' . $this->getHost();
        }
        $path = $this->getPath();
        if ($path) {
            if (empty($path) || '/' !== substr($path, 0, 1)) {
                $path = '/' . $path;
            }
            $uriString .= $path;
        }
        if ($this->getQuery()) {
            $uriString .= sprintf('?%s', $this->getQuery());
        }
        if ($this->getFragment()) {
            $uriString .= sprintf('#%s', $this->getFragment());
        }

        $this->cachedUriString = $uriString;

        return $this->cachedUriString;
    }
}
