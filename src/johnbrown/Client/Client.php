<?php
namespace johnbrown\Client;
use johnbrown\Framework\Messages\Interfaces\UriInterface;
use johnbrown\Framework\Messages\Uri;

/**
 * Class Client
 *
 * curl based client for sending requests to the api endpoints
 *
 * @package johnbrown
 * @subpackage Client
 */
class Client
{
    /**
     * @var array
     */
    private $handles = [];

    /**
     * @var
     */
    private $mutliHandle;


    /**
     * @param UriInterface $uri
     * @param array $headers
     * @param array $data
     * @return Client
     */
    public function createHandle(UriInterface $uri, array $headers = [], array $data = [])
    {
        if ($this->mutliHandle === null) {
            $this->mutliHandle = curl_multi_init();
        }

        $url = $uri->__toString();
        $handle = curl_init($url);
        curl_setopt($handle,CURLOPT_RETURNTRANSFER, 1);

        if (!empty($headers)) {
            $headerData = $this->convertHeaders($headers);
            curl_setopt($handle, CURLOPT_HTTPHEADER, $headerData);
        }

        if (!empty($data)) {
            $dataString = $this->convertData($data);
            curl_setopt($handle, CURLOPT_POST, count($data));
            curl_setopt($handle, CURLOPT_POSTFIELDS, $dataString);
        }

        $this->addHandle($url, $handle);

        return $this;
    }

    /**
     * @param array $headers
     * @return array
     */
    private function convertHeaders(array $headers)
    {
        $headerData = [];
        foreach($headers as $name => $value) {
            $headerData[] = sprintf('%s: %s', $name, $value);
        }
        return $headerData;
    }

    /**
     * @param array $data
     * @return string
     */
    private function convertData(array $data)
    {
        $fieldsString = '';
        foreach($data as $key=>$value) { $fieldsString .= $key.'='.$value.'&'; }
        rtrim($fieldsString, '&');

        return $fieldsString;
    }

    /**
     * exec
     */
    public function exec()
    {
        if (count($this->handles) == 0) {
            throw new \RuntimeException('No Handles have been added to exec');
        }

        // While we're still active, execute curl
        $active = null;
        do {
            $mrc = curl_multi_exec($this->mutliHandle, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            // Wait for activity on any curl-connection
            if (curl_multi_select($this->mutliHandle) == -1) {
                usleep(1);
            }

            // Continue to exec until curl is ready to
            // give us more data
            do {
                $mrc = curl_multi_exec($this->mutliHandle, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        return $this;
    }

    /**
     * readContent
     */
    public function readContent()
    {
        $content = [];
        foreach ($this->handles as $url => $handles) {
            foreach ($handles as $handle) {
                $content[$url][] = curl_multi_getcontent($handle); // get the content
            }
        }

        return $content;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasHandle($name)
    {
        return (isset($this->handles[$name]));
    }

    /**
     * @param $name
     * @param $handle
     */
    public function addHandle($name, $handle)
    {
        curl_multi_add_handle($this->mutliHandle, $handle);
        $this->handles[$name][] = $handle;
    }

    /**
     * close
     *
     * closes a particular handle
     *
     * @param string $name
     * @return Client
     */
    public function close($name)
    {
        if ($this->hasHandle($name)) {
            foreach ($this->handles[$name] as $handle) {
                curl_multi_remove_handle($this->mutliHandle, $handle);
                unset($this->handles[$name]);
            }
        }
        return $this;
    }

    public function closeHandles()
    {
        foreach ($this->handles as $name=>$handle) {
            $this->close($name);
        }
    }

    /**
     * closeAll
     *
     * @return Client
     */
    public function closeAll()
    {
        $this->closeHandles();
        curl_multi_close($this->mutliHandle);
        return $this;
    }
}