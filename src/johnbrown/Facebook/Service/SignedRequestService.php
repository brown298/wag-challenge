<?php
namespace johnbrown\Facebook\Service;

use johnbrown\Facebook\Exception\InvalidDataException;
use johnbrown\Facebook\Exception\InvalidSignatureException;

/**
 * Class SignedRequestService
 *
 * encodes and decodes facebook signed requests
 *
 * @package johnbrown
 * #subpackage Facebook\Service
 */
class SignedRequestService
{
    /**
     * @var string
     */
    private $appId;

    /**
     * @var string
     */
    private $secret;

    /**
     * SignedRequestService constructor.
     * @param string $appId
     * @param string $secret
     */
    public function __construct($appId, $secret)
    {
        $this->appId = $appId;
        $this->secret = $secret;
    }

    /**
     * @param string $signedRequest
     * @return mixed
     * @throws InvalidSignatureException
     */
    public function decodeSignedRequest($signedRequest)
    {
        list($encodedSig, $payload) = explode('.', $signedRequest, 2);
        $signature = $this->base64UrlDecode($encodedSig);
        $data = json_decode($this->base64UrlDecode($payload), true);

        $expectedSig = hash_hmac('sha256', $payload, $this->secret, true);
        if (!hash_equals($signature,$expectedSig)) {
            throw new InvalidSignatureException('Bad Signature');
        }

        return $data;
    }

    /**
     * create a mock signed request
     * @param array $data
     *
     * @return string
     */
    public function encodeSignedRequest(array $data = [])
    {
        $dataString = json_encode($data);
        $payload = $this->base64UrlEncode($dataString);
        $rawSignature = hash_hmac('sha256', $payload, $this->secret, true);
        $signature = $this->base64UrlEncode($rawSignature);

        return $signature . '.' . $payload;
    }

    /**
     * @param $signedRequest
     * @return mixed
     * @throws InvalidDataException
     */
    public function getSignedUserId($signedRequest)
    {
        $data = $this->decodeSignedRequest($signedRequest);
        if (!isset($data['user_id'])) throw new InvalidDataException('user_id was not provided by Facebook');

        return $data['user_id'];
     }

    /**
     * base64UrlDecode
     *
     * @param $data
     * @return bool|string
     */
    private function base64UrlDecode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * base64UrlDecode
     *
     * @param $data
     * @return bool|string
     */
    private function base64UrlEncode($data) {
        return str_replace('=', '', strtr(base64_encode($data), '+/', '-_'));
    }
}