<?php
namespace johnbrown\Facebook\Exception;

/**
 * Class InvalidSignatureException
 *
 * signature could not be verified
 *
 * @package johnbrown
 * @subpackage Facebook\Esception
 */
class InvalidSignatureException extends \Exception
{

}