<?php
namespace johnbrown\Facebook\Exception;

/**
 * Class InvalidDataException
 * @package johnbrown
 * @subpackage Facebook\Esception
 */
class InvalidDataException extends \Exception
{

}