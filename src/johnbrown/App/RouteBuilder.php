<?php
namespace johnbrown\App;

use johnbrown\Framework\Route\RouteBuilderInterface;
use johnbrown\Framework\Route\RouterInterface;

/**
 * Class RouteBuilder
 *
 * takes the controllers and defines routes for them
 *
 * @package johnbrown
 * @subpackage App
 */
class RouteBuilder implements RouteBuilderInterface
{
    /**
     * @var array of controllers to be used
     */
    private $controllers;

    public function __construct(
        array $controllers
    )
    {
        $this->controllers = $controllers;
    }

    /**
     * define all the application routes here
     * @return mixed
     */
    public function buildRoutes(RouterInterface $router)
    {
        foreach ($this->controllers as $controller) {
            $controller->buildRoutes($router);
        }

        // add any custom routes here...
    }
}