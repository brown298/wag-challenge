<?php
namespace johnbrown\App\Service;

use johnbrown\App\Entity\Scores;
use johnbrown\Framework\Database\EntityManager;
use johnbrown\Framework\Exception\QueryException;

/**
 * Class PlayerInfoService
 *
 * determines info for players to build the reports
 *
 * @package johnbrown
 * @subpackage App\Service
 */
class PlayerInfoService
{
    const START_OF_DAY_TIME = ' 00:00:00';
    const END_OF_DAY_TIME   = ' 23:59:59';

    /**
     * @var EntityManager
     */
    private $entityManger;

    /**
     * PlayerCountService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManger = $entityManager;
    }

    /**
     * countPlayers
     * @return mixed
     * @throws QueryException
     */
    public function countPlayers()
    {
        $stmt   = $this->entityManger->prepare('select count(id) as count FROM `' . Scores::TABLE_NAME . '`');
        $result = $this->entityManger->fetchAssoc($stmt);

        if (!$result) {
            Throw new QueryException('Unknown Error counting players');
        }

        return $result['count'];
    }

    /**
     * countPlayersByDate
     *
     * @param \DateTime $date
     * @return int
     * @throws QueryException
     */
    public function countPlayersByDate(\DateTime $date)
    {
        $nextDay = clone $date;
        $nextDay->add(new \DateInterval('P1D'));
        $stmt   = $this->entityManger->prepare('select count(id) as count FROM `' . Scores::TABLE_NAME
            . '` WHERE date_modified >= :startDate and date_modified < :endDate');
        $stmt->bindValue(':startDate',($date->format('Y-m-d') . ' 00:00:00'));
        $stmt->bindValue(':endDate', ($nextDay->format('Y-m-d') . ' 00:00:00'));
        $result = $this->entityManger->fetchAssoc($stmt);

        if (!$result) {
            Throw new QueryException('Unknown Error counting players');
        }

        return $result['count'];
    }

    /**
     * gets an ordered list of players
     *
     * @param int $numPlayers
     * @param string $sortdir
     * @return array
     * @throws QueryException
     */
    public function getTopPlayers($numPlayers, $sortdir = 'desc')
    {
        if ($sortdir !== 'desc') $sortdir = 'asc';

        $stmt   = $this->entityManger->prepare('select user_id, score, date_modified FROM `' . Scores::TABLE_NAME
            . '` ORDER BY `score` ' . $sortdir . ' LIMIT ' . intval($numPlayers));
        $result = $this->entityManger->fetchAllAssoc($stmt);
        if (!$result) {
            Throw new QueryException('Unknown Error loading players');
        }

        return $result;
    }

    /**
     * @param $numPlayers
     * @param string $sortdir (asc or desc)
     * @return array
     * @throws QueryException;
     */
    public function getMostImprovedPlayers($numPlayers, $sortdir = 'desc')
    {
        if ($sortdir !== 'desc') $sortdir = 'asc';
        $priorMonday = date('Y-m-d', strtotime('last week monday'));
        $priorSunday = date('Y-m-d', strtotime('last week sunday'));

        $sql = 'select user_id, score as this_week_score, prior_week_score as last_week_score, improvement as score_diff'
            . ' FROM `' . Scores::TABLE_NAME . '` WHERE prior_week_score_date >= :lastMonday AND prior_week_score_date <= :lastSunday'
            . ' ORDER BY improvement ' . $sortdir . ' LIMIT 10'
        ;

        $stmt   = $this->entityManger->prepare($sql);
        $stmt->bindValue('lastMonday', $priorMonday . self::START_OF_DAY_TIME);
        $stmt->bindValue('lastSunday', $priorSunday . self::END_OF_DAY_TIME);

        $result = $this->entityManger->fetchAllAssoc($stmt);
        if ($result === false) {
            $errorInfo = $stmt->errorInfo();var_dump($errorInfo);
            Throw new QueryException(sprintf('Error loading players: %s', implode(',', $errorInfo)));
        }

        return $result;
    }

}
