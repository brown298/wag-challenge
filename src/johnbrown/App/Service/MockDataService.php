<?php
namespace johnbrown\App\Service;

use johnbrown\App\Entity\Scores;
use johnbrown\Client\Client;
use johnbrown\Facebook\Service\SignedRequestService;
use johnbrown\Framework\Database\EntityInterface;
use johnbrown\Framework\Database\EntityManager;
use johnbrown\Framework\Messages\Uri;

/**
 * Class MockDataService
 *
 * creates mock data and either saves to the database or sends to an api endpoint
 *
 * @package johnbrown
 * @subpakcage App\Service
 */
class MockDataService
{
    const EXAMPLE_SIGNED_REQUEST = "cjv1NZlSRCthYq9rAyWEidD7QE98p0PKZvVwpQ7gPwg.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImV4cGlyZXMiOjEzMjI4NTYwMDAsImlzc3VlZF9hdCI6MTMyMjg1MDc1NCwib2F1dGhfdG9rZW4iOiJBQUFCelMwYVhTMDBCQUlob0I1bmhrYnZJU0xLSGpNb3ZIN2ZTTmMzWkFxbnVNT2NvYmpJUHoxNGFmWXV1dzBkbkZzeVpBV2JHU2MycXZBakdjRzZUQ1RWZzBLOUVGUWJ5WkJwNTU0ZXE5M2FTWkFXZXpVeEYiLCJ1c2VyIjp7ImNvdW50cnkiOiJ1cyIsImxvY2FsZSI6ImVuX1VTIiwiYWdlIjp7Im1pbiI6MjF9fSwidXNlcl9pZCI6IjEwMDAwMzI5MTY2MTkwOSJ9";

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var SignedRequestService
     */
    private $signedRequestService;

    /**
     * @var string of the host
     */
    private $host;

    /**
     * MockDataService constructor.
     * @param EntityManager $em
     * @param Client $client
     * @param SignedRequestService $signedRequestService
     * @param $host
     */
    public function __construct(
        EntityManager $em,
        Client $client,
        SignedRequestService $signedRequestService,
        $host
    )
    {
        $this->host = $host;
        $this->em      = $em;
        $this->client  = $client;
        $this->signedRequestService = $signedRequestService;
    }

    /**
     * creates the raw data for
     *
     * @param $iteration
     * @return Scores
     */
    public function createMockData($iteration)
    {
        $iteration = intval($iteration);
        $userId =  "1000032000" . ($iteration);
        $score  = mt_rand();
        $date   = $this->getMockDate(0,7);

        $score = new Scores($userId, $score);
        $score->setDateModified(new \DateTime($date));
        return $score;
    }

    /**
     * @param int $start
     * @param int $end
     * @return string
     */
    private function getMockDate($start, $end)
    {
        return sprintf('%s %02d:%02d:%02d', date('Y-m-d', strtotime( '-'.mt_rand($start,$end).' days')), mt_rand(0,23), mt_rand(0,59), mt_rand(0,59));;
    }

    /**
     * createAndSave
     *
     * creates mock data and saves to database
     *
     * create mock data and save to database
     * @param $iteration
     * @return Scores
     */
    public function createAndSave($iteration)
    {
        $entity = $this->createMockData($iteration);
        $this->em->save($entity);
        return $entity;
    }

    /**
     * getNewScoreForUserId
     *
     * @param $userId
     * @return Scores
     */
    public function getNewScoreForUserId($userId)
    {
        $scoreValue  = mt_rand();
        $date   = $this->getMockDate(7,14);

        $score = new Scores($userId, $scoreValue);
        $score->setDateModified(new \DateTime($date));
        return $score;
    }

    /**
     * @param Scores $score
     */
    public function addScoreToUserAndSave(Scores $score)
    {
        $newScore = $this->getNewScoreForUserId($score->getUserId());
        $this->save($newScore);
    }

    /**
     * @param EntityInterface $entity
     * @return EntityInterface
     */
    public function save(EntityInterface $entity)
    {
        $this->em->save($entity);
        return $entity;
    }

    /**
     * createAndSend
     *
     * creates Mock data and sends it to the api
     *
     * @param $iteration
     * @return array
     */
    public function createAndSend($iteration, $numberOfRequests = 1, $percentRepeat = 0)
    {
        for ($x = 0; $x < $numberOfRequests; $x++) {
            $iteration = ($iteration*$numberOfRequests + $x);
            $score = $this->createMockData($iteration);
            $request = $this->buildRequest($score);
            if (mt_rand(0,100) > $percentRepeat && ($x+1) < $numberOfRequests) {
                $newScore = $this->getNewScoreForUserId($score->getUserId());
                $request = $this->buildRequest($newScore);
                $x++;
            }
        }
        if (!isset($request)) return [];

        $content = $request->exec()
            ->readContent();
        $this->client->closeHandles();
        return $content;
    }

    /**
     * @param Scores $score
     * @return Client
     */
    public function buildRequest(Scores $score)
    {
        $exampleData = $this->signedRequestService->decodeSignedRequest(self::EXAMPLE_SIGNED_REQUEST);

        $uri = (new Uri())
            ->withScheme('http')
            ->withHost($this->host)
            ->withPath('/api/v1/score')
        ;

        unset($exampleData['user_id']);
        $exampleData['user_id'] = (string)$score->getUserId();

        $data = [
            'signed_request' => $this->signedRequestService->encodeSignedRequest($exampleData),
            'user_score'     => $score->getScore(),
            'date'           => $score->getDateModified()->format('Y-m-d H:i:s'),
        ];
        $request = $this->client->createHandle($uri,[], $data);

        return $request;
    }
}