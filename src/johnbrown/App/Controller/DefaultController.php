<?php
namespace johnbrown\App\Controller;

use johnbrown\App\Entity\Scores;
use johnbrown\App\Service\PlayerInfoService;
use johnbrown\Facebook\Exception\InvalidSignatureException;
use johnbrown\Facebook\Service\SignedRequestService;
use johnbrown\Framework\AbstractController;
use johnbrown\Framework\Database\EntityManager;
use johnbrown\Framework\Exception\MissingArgumentException;
use johnbrown\Framework\Exception\QueryException;
use johnbrown\Framework\Messages\Interfaces\RequestInterface;
use johnbrown\Framework\Messages\Interfaces\ResponseInterface;
use johnbrown\Framework\Messages\JsonResponse;
use johnbrown\Framework\Messages\ServerRequest;

/**
 * Class DefaultController
 *
 * controller exposing a high scores tracking api
 *
 * @package johnbrown
 * @subpackage App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @var array routes for this controller
     */
    protected $routes = [
        [ 'path' => '/^\/api\/v1\/score$/', 'action' => 'addScoreAction', 'methods' => ['POST', 'PUT'] ],
        [ 'path' => '/^\/api\/v1\/scoreboard$/', 'action' => 'getScoreboardAction', 'methods' => ['GET'] ],
    ];

    /**
     * creates a scoreboard report
     *
     * @param RequestInterface $request
     * @return JsonResponse
     */
    public function getScoreboardAction(RequestInterface $request)
    {
        $displayPlayers = $this->getContainer()->getParameter('scoreboard.display_players');
        if ( !($request instanceof ServerRequest)) Throw new \RuntimeException('Invalid Request, expected ServerRequest');
        $today = new \DateTime();

        /** @var PlayerInfoService $playerInfoService */
        $playerInfoService = $this->getContainer()->get('player_info_service');
        try {
            $topPlayers    = $playerInfoService->getTopPlayers($displayPlayers);
            $mostImproved  = $playerInfoService->getMostImprovedPlayers($displayPlayers);
            $totalPlayers  = $playerInfoService->countPlayers();
            $activePlayers = $playerInfoService->countPlayersByDate($today);
        } catch (QueryException $ex) {
            /** @todo log the reason */
            $message = 'Unexpected Database Error';
            return new JsonResponse(['success' => false, 'reason' => $message], 500, $message);
        }

        return new JsonResponse([
            'total_players'         => $totalPlayers,
            'active_players'        => $activePlayers,
            'top_players'           => $topPlayers,
            'most_improved_players' => $mostImproved,
        ]);
    }

    /**
     * takes a post and adds a high score
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws MissingArgumentException
     */
    public function addScoreAction(RequestInterface $request)
    {
        if ( !($request instanceof ServerRequest)) Throw new \RuntimeException('Invalid Request, expected ServerRequest');
        $data = $request->getParsedBody();

        /** @var SignedRequestService $signedRequestService */
        $signedRequestService = $this->getContainer()->get('facebook.signed_request_service');
        try {
            $userId = $signedRequestService->getSignedUserId($data['signed_request']);
        } catch (InvalidSignatureException $ex) {
            return new JsonResponse(['success' => false, 'reason' => $ex->getMessage()], 403, $ex->getMessage());
        }

        if (!isset($data['user_score'])) {
            $reason = 'Missing argument user_score';
            return new JsonResponse(['success' => false, 'reason' => $reason], 400, $reason);
        }

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('entity_manager');
        $score = new Scores($userId, $data['user_score']);

        if (isset($data['date'])) { // force the date
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $data['date']);
            $score->setDateModified($date);
        }

        try {
            $em->save($score);
        } catch (QueryException $ex) {
            /** @todo log actual error */
            return new JsonResponse(['success' => false, 'reason' => 'unknown error saving to database'], 500, 'unknown error saving to database');
        }

        return new JsonResponse([
            'success' => true,
        ]);
    }
}
