<?php
namespace johnbrown\App\Entity;

use johnbrown\Framework\Database\EntityInterface;
use johnbrown\Framework\Database\EntityTrait;
use johnbrown\Framework\Exception\QueryException;

/**
 * Class Scores
 *
 * defines an ORM Entity for a score
 *
 * @package johnbrown
 * @subpackage Entity
 */
class Scores implements EntityInterface
{
    use EntityTrait;
    const TABLE_NAME = 'scores';

    /**
     * @var int|null
     */
    public $id;

    /**
     * @var int|null
     */
    public $user_id;

    /**
     * @var int|null
     */
    public $score;

    /**
     * @var \DateTime|null
     */
    public $date_modified;

    /**
     * @var int|null
     */
    public $prior_week_score;

    /**
     * @var \DateTime|NULL
     */
    public $prior_week_score_date;

    /**
     * @var int|null
     */
    public $improvement;

    /**
     * Scores constructor.
     * @param integer $userId
     * @param integer $score
     */
    public function __construct( $userId = null, $score = null)
    {
        if ($userId !== null) $this->user_id = $userId;
        if ($score !== null) $this->score    = $score;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int|null $userId
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
    }

    /**
     * @return int|null
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param int|null $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }


    /**
     * @return \DateTime|nul
     */
    public function getDateModified()
    {
        return $this->date_modified;
    }

    /**
     * @param \DateTime|nul $date_modified
     */
    public function setDateModified($date_modified)
    {
        $this->date_modified = $date_modified;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return self::TABLE_NAME;
    }

    /**
     * @return int|null
     */
    public function getPriorWeekScore()
    {
        return $this->prior_week_score;
    }

    /**
     * @param int|null $prior_week_score
     */
    public function setPriorWeekScore($prior_week_score)
    {
        $this->prior_week_score = $prior_week_score;
    }

    /**
     * @return \DateTime|NULL
     */
    public function getPriorWeekScoreDate()
    {
        return $this->prior_week_score_date;
    }

    /**
     * @param \DateTime|NULL $prior_week_score_date
     */
    public function setPriorWeekScoreDate($prior_week_score_date)
    {
        $this->prior_week_score_date = $prior_week_score_date;
    }


    /**
     * @return mixed
     * @throws QueryException
     */
    public function createTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `" . $this->getTableName() . "` ("
            . "`id` BIGINT(10) unsigned NOT NULL AUTO_INCREMENT,"
            . "`user_id` BIGINT(10) unsigned NOT NULL,"
            . "`score` INT(10) unsigned NOT NULL,"
            . "`date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,"
            . "`prior_week_score` INT(10) unsigned DEFAULT NULL,"
            . "`improvement` INT(10) DEFAULT NULL,"
            . "`prior_week_score_date` timestamp NULL DEFAULT NULL,"
            . "PRIMARY KEY (`id`),"
            . "KEY `k_score` (`score`),"
            . "KEY `k_user_id` (`user_id`),"
            . "KEY `k_date_modified` (`date_modified`),"
            . "KEY `k_prior_week_date` (`prior_week_score_date`),"
            . "KEY `k_improvement` (`improvement`)"
            . ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC";
        $stmt =  $this->connection->prepare($sql);
        $result = $stmt->execute();
        if (!$result) {
            $errorInfo = $stmt->errorInfo();
            throw new QueryException(sprintf('Error saving to database: %s', implode(',', $errorInfo)));
        }
        return $result;
    }

    /**
     * saves a record to the databse
     *
     * @return mixed
     * @throws QueryException
     */
    public function save()
    {

        $columns = ['user_id', 'score'];
        if ($this->getDateModified() !== null) $columns[] = 'date_modified';

        $priorWeek = $this->getPriorWeekScoreForUser($this->user_id);
        if ($priorWeek instanceof Scores) {
            $columns['prior_week_score'] = 'prior_week_score';
            $columns['prior_week_score_date'] = 'prior_week_score_date';
            $columns['improvement'] = 'improvement';
        }

        $sql = "INSERT INTO `" . $this->getTableName() . "` (" . implode(', ', $columns) . ") VALUES ( :id, :score"
            . (($this->getDateModified() !== null) ? ', :dateModified' : '')
            . ((isset($columns['prior_week_score'])) ? ', :priorScore' : '')
            . ((isset($columns['prior_week_score_date'])) ? ', :priorScoreDate' : '')
            . ((isset($columns['improvement'])) ? ', :improvement' : '')
            . ")";
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':id', $this->user_id);
        $stmt->bindParam(':score', $this->score);

        if ($this->getDateModified() !== null) $stmt->bindValue(':dateModified', $this->getDateModified()->format('Y-m-d H:i:s'));
        if (isset($columns['prior_week_score'])) $stmt->bindValue(':priorScore', $priorWeek->getScore());
        if (isset($columns['prior_week_score_date'])) $stmt->bindValue(':priorScoreDate', $priorWeek->getDateModified());
        if (isset($columns['improvement'])) $stmt->bindValue(':improvement', ($this->getScore() - $priorWeek->getScore()));

        $result = $stmt->execute();
        if (!$result) {
            $errorInfo = $stmt->errorInfo();
            throw new QueryException(sprintf('Error saving to database: %s', implode(',', $errorInfo)));
        }
        return $result;
    }

    /**
     * @param $userId
     * @return mixed
     */
    private function getPriorWeekScoreForUser($userId)
    {
        $priorMonday = date('Y-m-d', strtotime('last week monday'));
        $priorSunday = date('Y-m-d', strtotime('last week sunday'));
        $sql = "SELECT * FROM `" . $this->getTableName()
            . "`  WHERE date_modified >= :lastMonday AND date_modified <= :lastSunday and user_id = :userId ORDER BY score DESC LIMIT 1";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':userId', $this->user_id);
        $stmt->bindValue(':lastMonday', $priorMonday . ' 00:00:00');
        $stmt->bindValue(':lastSunday', $priorSunday . ' 23:59:59');
        $stmt->setFetchMode(\PDO::FETCH_CLASS, get_class($this));
        $stmt->execute();
        return $stmt->fetch();
    }

    /**
     * remove
     *
     * @return mixed
     */
    public function remove()
    {
        if ($this->getId() == null) return; //cowardly refusing to do nothing

        $sql = "DELETE FROM `" . $this->getTableName() . "` WHERE user_id = :id";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':id', $this->user_id);
        return $stmt->execute();
    }

    /**
     * load
     * @param array $data
     * @return Scores
     */
    public function load(array $data)
    {
        if (isset($data['id'])) $this->setUserId(intval($data['id']));
        if (isset($data['score'])) $this->setScore(intval($data['score']));
        if (isset($data['date_modified'])) $this->setDateModified(new \DateTime($data['date_modified']));
        if (isset($data['prior_week_score'])) $this->setPriorWeekScore($data['prior_week_score']);
        if (isset($data['prior_week_score_date'])) $this->setPriorWeekScoreDate(new \DateTime($data['prior_week_score_date']));
        return $this;
    }


}