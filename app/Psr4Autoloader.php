<?php

/**
 * Class Psr4Autoloader
 *
 * psr-4 compatible autoloader
 */
class Psr4Autoloader
{
    /**
     * @var array
     */
    protected $prefixes = [];

    /**
     * register the loader
     */
    public function register()
    {
        spl_autoload_register([$this, 'loadClass']);
    }

    /**
     * add a namespace to the autoloader
     * @param string $prefix
     * @param string $baseDir
     * @param bool  $prepend
     * @internal param string $prifix
     */
    public function addNamespace($prefix, $baseDir, $prepend=false)
    {
        // normalize input
        $prefix  = trim($prefix, '\\') . '\\';
        $baseDir = rtrim($baseDir, DIRECTORY_SEPARATOR) . '/';
        // init namespace prefix array
        if (isset($this->prefixes[$prefix]) === false) {
            $this->prefixes[$prefix] = [];
        }

        // update the prefixes array
        if ($prepend) {
            array_unshift($this->prefixes[$prefix], $baseDir);
        } else {
            array_push($this->prefixes[$prefix], $baseDir);
        }
    }

    /**
     * loads the class for a given class name
     *
     * @param string $class the class name to load
     * @return mixed
     */
    public function loadClass($class)
    {
        $prefix = $class;
        while(false !== $pos = strpos($prefix,'\\')) {
            $prefix = substr($class, 0, $pos +1);
            $relativeClass = substr($class, $pos+1);
            $mappedFile = $this->loadMappedFile($prefix, $relativeClass);
            if ($mappedFile) return $mappedFile;
            $prefix = rtrim($prefix, '\\');
        }
        return false;
    }

    /**
     * loads a mapped file
     *
     * @param string $prefix
     * @param string $relativeClass
     * @return mixed
     */
    public function loadMappedFile($prefix, $relativeClass)
    {
        if (isset($this->prefixes[$prefix]) === false) return false;
        foreach ($this->prefixes[$prefix] as $baseDir) {
            $file = $baseDir . str_replace('\\', '/', $relativeClass) . '.php';
            if ($this->requireFile($file)) return $file; 
        }
        return false;
    }

    /**
     * requires the selected file
     * 
     * @param string $file
     * @return bool
     */
    protected function requireFile($file)
    {
        if (file_exists($file)) {
           require_once($file);
           return true;
        }
        return false;
    }
}
