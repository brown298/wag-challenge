<?php
/**
 * defines an autoloader for the application and registers namespaces
 */
require_once('Psr4Autoloader.php');
$psr4Autoload = new Psr4Autoloader();

// register namespaces
$psr4Autoload->addNamespace('johnbrown', __DIR__ . '/../src/johnbrown');

// add the autoloader
$psr4Autoload->register();

